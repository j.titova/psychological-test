
let result1 = [];
let answer1 = [];
let seconds1;

function startTest1(id_title, id_subtitle, btn) {
    let circles = [1, 2, 3, 4, 5, 6, 7];
    document.getElementById(id_title).style.visibility="hidden";
    document.getElementById(id_subtitle).style.visibility="hidden";
    btn.style.display='none';

    shuffle(circles);
    makeRed(0, circles);
    setTimeout(
        () => {
            for (let i = 1; i < 8; i++)
                document.getElementById(i.toString()).style.background="#222222";
            document.getElementById('task1').style.display="block";
            startTimer();
        }
        , 8000);
}

function finishTest1() {
    stopTimer();
    seconds1 = seconds;
    seconds = 0;
    document.getElementById('test1').style.display="none";
    document.getElementById('result1').style.display="flex";
    let result = [];
    let answer = [];
    for (let i = 0; i < 7; i++){
        result.push(result1[i]);
        answer.push(answer1[i]);
    }
    document.getElementById('resOut_1').innerHTML = result.toString();
    document.getElementById('ansOut_1').innerHTML = answer.toString();
    document.getElementById('time_1').innerHTML = seconds1;

    data.test_1.result = result;
    data.test_1.answer = answer;
    data.test_1.time = seconds1;
}

function chooseCircle(circle_id){
    let circle = document.getElementById(circle_id);
    //let color = window.getComputedStyle(circle).getPropertyValue("background");
    if (circle.style.backgroundColor == "rgb(34, 34, 34)"){
        circle.style.color = "rgb(34, 34, 34)"
        circle.style.background = "#9E0D16";
        result1.push(parseInt(circle.id));
        circle.innerHTML = result1.length.toString();
    }
    else if (circle.id === result1[result1.length - 1]) {
        circle.style.background="#222222";
        result1.pop();
    }

    if (result1.length === 7) {
        document.getElementById('finishTask1').style.display="block";
        document.getElementById('task1').style.display="none";
    } else {
        document.getElementById('finishTask1').style.display="none";
        //document.getElementById('task1').style.display="block";
    }
}

function makeRed(i, circles) {
    setTimeout(function() {
        document.getElementById(circles[i]).style.background="#9E0D16";
        answer1.push(circles[i]);
        i++;
        if (i < 7) makeRed(i, circles);
    }, 1000)
}

