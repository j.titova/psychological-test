
let login = prompt("Введите логин");
let regex = /[s]\d\d\d\d\d\d\d/;
while (login == null || !login.match(regex)) {
  alert("Неверный логин");
  login = prompt("Введите логин");
}

data.login = login;

let time = 0;
let seconds = 0;

function add() {
  seconds++;
  timer();
}

function timer () {
  time = setTimeout(add, 1000);
}

function startTimer () {
  timer();
}

function stopTimer () {
  clearTimeout(time);
}
 
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }

  if(isArrayOrdered(array)) shuffle(array);
}

function nextStage (id_show, id_hide, display) {
  document.getElementById(id_show).style.display=display;
  document.getElementById(id_hide).style.display='none';
}

